# 半自动化获取B站 粉丝列表




![案例](_V__%E5%A4%A7%E5%9E%8B%E6%A0%8F%E7%9B%AE-%E5%B9%B4%E6%9C%AB%E6%B7%B7%E5%89%AA2022_JGetFans_Face_banner.html%20(1).png)




###   使用教程

#### 第一步

这一套源码 
先使用Idea 
创建一个普通的Maven项目和将Pom.xml的前置加载
其pom.xml导入写
(都是官方库子的东西)


        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.0</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.38</version>
        </dependency>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.9.2</version>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.17</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.25</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.7.25</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.25</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.14.3</version>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-chrome-driver</artifactId>
            <version>4.1.0</version>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-server</artifactId>
            <version>4.0.0-alpha-2</version>
        </dependency>`



#### 第二步

然后运行

去你Chrome根目录下

打开CMD 输入

`chrome.exe --remote-debugging-port=9527 --user-data-dir=“D:\selenium\AutomationProfile”`

然后浏览器打开了登录你的B站号

然后你就可以运行

` GetFans.java `

然后记得设置一下本地html的路径 开始半手动下载html

你也可以添加进去详细见chromedrive的link操作



#### 第三步

本地已经有了HTML了后运行 记得修改html所在文件夹

`read.py`

来批量粉丝粉丝ID与头像 以及他的空间




#### 第四步

如果你是渲染列表

用在rename.java批量修改文件名字后

用index.html渲染出来

这个时候截图就好了