import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 *
 * @implNote 半自动化将Html下载到本地
 * @author LyxyCraft
 *
 */
public class GetFans {
    static String prefix = "[LyxyCraft_B站粉丝列表获取]";
    private String source;

    public static void main(String[] args) throws InterruptedException, IOException {
        //连接浏览器
        for (int i=1; i<36; i++) {
            write(connweb(), i);
            System.out.println("请切换下页3秒后捕捉下一页");
            Thread.sleep(3000);
        }
    }


//    static void sort(WebDriver H) {
////        List<WebElement> content = H.findElement(By.xpath("/html/body/div/div/div/div/div/div/div/div/ul/li"));
//        List<WebElement> content = H.findElements(By.cssSelector("#page-follows li"));;
//        for (WebElement get : content) {
//            //获取文章标题
//            String url = get.findElement(By.xpath("/div/a/img")).getAttribute("src");
//            //获取获取帖子网址
//            String name = get.findElement(By.xpath("/div/a/span")).getText();
//        }
//    }

    static void write(String info, int ram) throws IOException {
        String folder = "V:\\大型栏目-年末混剪2022\\JGetFans\\List";
        File files = new File(folder);
        files.mkdirs();
        File file = new File(folder + File.separator + "第" + ram + "页的连接数据" +  ".txt");
        file.createNewFile();
        BufferedWriter out = new BufferedWriter(new FileWriter(file));
        out.write(info);
        System.out.println(ram + "爬完");
        out.flush();
    }

    public static String connweb() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("debuggerAddress", "127.0.0.1:9527");
        WebDriver driver = new ChromeDriver(options);
        System.out.println(prefix + "连接完成！10秒后开始下一步操作 请打开对应网页");
        for (int i=10; i>0; --i) {
            System.out.println(prefix + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return driver.getPageSource();
    }

}
